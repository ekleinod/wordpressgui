# WordpressGUI

Simple Wordpress GUI.

## Git-Repository

The branching model regards to the stable mainline model described in <http://www.bitsnbites.eu/a-stable-mainline-branching-model-for-git>

This means, there is always a stable mainline, the `master` branch.

## Legal stuff

License: GNU General Public License.
See file [COPYING](COPYING).

